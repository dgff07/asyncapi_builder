import sys
import json
import time

def getPadding(numberSpaces):
    return " " * numberSpaces

def getJsonContent(file_path):
    with open(file_path) as json_file:
        return json.load(json_file)

def defineStringProperty(padding, propertyName, ignorePropertyName):
    paddingString = getPadding(padding)
    propertyName = "" if ignorePropertyName else paddingString+propertyName+":\n"
    return propertyName+paddingString+"  type: string\n"


def defineIntegerProperty(padding, propertyName, ignorePropertyName):
    paddingString = getPadding(padding)
    propertyName = "" if ignorePropertyName else paddingString+propertyName+":\n"
    return propertyName+paddingString+"  type: integer\n"+paddingString+"  format: int32\n"


def defineLongProperty(padding, propertyName, ignorePropertyName):
    paddingString = getPadding(padding)
    propertyName = "" if ignorePropertyName else paddingString+propertyName+":\n"
    return propertyName+paddingString+"  type: integer\n"+paddingString+"  format: int64\n"


def defineFloatProperty(padding, propertyName, ignorePropertyName):
    paddingString = getPadding(padding)
    propertyName = "" if ignorePropertyName else paddingString+propertyName+":\n"
    return propertyName+paddingString+"  type: number\n"+paddingString+"  format: float\n"


def defineDoubleProperty(padding, propertyName, ignorePropertyName):
    paddingString = getPadding(padding)
    propertyName = "" if ignorePropertyName else paddingString+propertyName+":\n"
    return propertyName+paddingString+"  type: number\n"+paddingString+"  format: double\n"


def defineBooleanProperty(padding, propertyName, ignorePropertyName):
    paddingString = getPadding(padding)
    propertyName = "" if ignorePropertyName else paddingString+propertyName+":\n"
    return propertyName+paddingString+"  type: boolean\n"

def defineObjectProperty(padding, propertyName, ignorePropertyName):
    paddingString = getPadding(padding)
    propertyName = "" if ignorePropertyName else paddingString+propertyName+":\n"
    return propertyName+paddingString+"  type: object\n"+paddingString+"  properties:\n"

def defineArrayOfSimpleTypesProperty(padding, propertyName, valueType):
    paddingString = getPadding(padding)

    typeString = {
        int : "integer",
        float : "float",
        str : "string",
        type(True) : "boolean",
        type(False) : "boolean" 
    }[valueType]
    return  paddingString+propertyName+":\n"+paddingString+"  type: array\n"+paddingString+"  items:\n"+paddingString+"    type: "+ typeString+"\n"

def defineArrayOfObjectsProperty(padding, propertyName):
    paddingString = getPadding(padding)

    return  paddingString+propertyName+":\n"+paddingString+"  type: array\n"+paddingString+"  items:\n"+paddingString+"    type: object\n"+paddingString+"    properties:\n"

def defineYaml(result, jsonContent, padding, ignorePropertyName=False):
    for propertyName in jsonContent:
        if(type(jsonContent[propertyName]) == type(True) or type(jsonContent[propertyName]) == type(False)):
            result += defineBooleanProperty(padding, propertyName, ignorePropertyName)
            continue
        if(isinstance(jsonContent[propertyName], int)):
            result += defineIntegerProperty(padding, propertyName, ignorePropertyName)
            continue
        if(isinstance(jsonContent[propertyName], str)):
            result += defineStringProperty(padding, propertyName, ignorePropertyName)
            continue
        if(isinstance(jsonContent[propertyName], float)):
            result += defineFloatProperty(padding, propertyName, ignorePropertyName)
            continue
        if(isinstance(jsonContent[propertyName], dict)):
            result += defineObjectProperty(padding, propertyName, ignorePropertyName)
            result = defineYaml(result, jsonContent[propertyName], padding+4)
            continue
        if(isinstance(jsonContent[propertyName], list)):
            if(not isinstance(jsonContent[propertyName][0], dict)):
                result += defineArrayOfSimpleTypesProperty(padding, propertyName, type(jsonContent[propertyName][0]))
            else:
                result += defineArrayOfObjectsProperty(padding, propertyName)
                result = defineYaml(result, jsonContent[propertyName][0], padding+6)

    return result

jsonContent = getJsonContent(sys.argv[1])
result = ""

result = defineYaml(result, jsonContent, 2)


print(result)

sys.exit()